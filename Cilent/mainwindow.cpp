﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "common.h"
#include "Client.h"
#include <QIntValidator>
#include <QMessageBox>
#include <QMouseEvent>
#include <QPainter>
#include <QNetworkInterface>

//注意：连接服务器的代码在子界面WidgetGame构造函数内

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this); 

	//ui->editIP->setText(GetIP());
	ui->editIP->setText(FIR_SERVER_IP);
    ui->editPort->setText(QString::number(FIR_SERVER_PORT));
    ui->editRoom->setText(QString::number(FIR_DEFAULT_ROOM));
    ui->editInput->installEventFilter(this);

    //限制端口编辑框输入格式：0-65535之间的整数
    QIntValidator* pInt = new QIntValidator(0, 65535, this);
    ui->editPort->setValidator(pInt);

    //房间号可输 0-99
    QIntValidator* pInt2 = new QIntValidator(0, 99, this);
    ui->editRoom->setValidator(pInt2);

    mGameWindow = ui->gameWnd;
    connect(mGameWindow,&GameWnd::sigOutput,this,&MainWindow::slotOutput);
    connect(mGameWindow,&GameWnd::sigUpdateRoomState,this,&MainWindow::slotUpdateRoomState);
}

MainWindow::~MainWindow()
{
    delete ui;
}

QString MainWindow::GetIP()
{
	QList<QHostAddress> list = QNetworkInterface::allAddresses();
	foreach(QHostAddress address, list)
	{
		if (address.protocol() == QAbstractSocket::IPv4Protocol)
		{
			return address.toString();
		}
	}
	return "";
}

void MainWindow::on_btnConnect_clicked()
{
    QString ip = ui->editIP->text();
    int port = ui->editPort->text().toInt();
    int room = ui->editRoom->text().toInt();
    mGameWindow->Connect(ip, port, room);
}

void MainWindow::on_btnSend_clicked()
{
    QString text = ui->editInput->toPlainText();
    mGameWindow->Say(text);
    ui->editInput->clear();
}

void MainWindow::slotOutput(QString text)
{
    ui->editShow->append(text);
}

void MainWindow::slotUpdateRoomState()
{
    QList<ROOM_STATE> rooms;
    mGameWindow->GetRoomState(rooms);

	ui->listRoom->clear();
	for (int i = 0; i < rooms.size(); i++)
    {
		ROOM_STATE r = rooms.at(i);
		QString name1 = QString::fromLocal8Bit(r.szName1);
		QString name2 = QString::fromLocal8Bit(r.szName2);

		QString text;
        int nPlayerInRoom = r.nPlayers;
        if(nPlayerInRoom == 0)
        {
            text = QStringLiteral("房间%1：空闲").arg(i);
        }
        else if(nPlayerInRoom == 1)
        {

            text = QStringLiteral("房间%1：一位玩家已进入(%2)").arg(i).arg(name1);
        }
        else
        {
            text = QStringLiteral("房间%1：房间已满！%2、%3").arg(i).arg(name1).arg(name2);
        }

        ui->listRoom->addItem(text);
    }
}

bool MainWindow::eventFilter(QObject *target, QEvent *event)
{
    if (target == ui->editInput)
    {
        if (event->type() == QEvent::KeyPress)
        {
            QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
            if (keyEvent->key() == Qt::Key_Enter || keyEvent->key() == Qt::Key_Return)
            {
                on_btnSend_clicked();
                return true;
            }
        }
    }
    return QMainWindow::eventFilter(target, event);
}
