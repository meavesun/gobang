
TEMPLATE = app

QT += core network widgets gui

CONFIG += debug_and_release
CONFIG(debug, debug|release){
    TARGET = ../../_debug/FIRClient
} else {
    TARGET = ../../_release/FIRClient
}

MOC_DIR += GeneratedFiles
OBJECTS_DIR += GeneratedFiles
UI_DIR += GeneratedFiles
RCC_DIR += GeneratedFiles

HEADERS += \
    ../Common/common.h \
    Client.h \
    Item.h \
    form.h \
    mainwindow.h \
    GameWnd.h \
    newmainwindow.h

SOURCES += \
    Client.cpp \
    GameModel.cpp \
    Item.cpp \
    form.cpp \
    main.cpp \
    mainwindow.cpp \
    GameWnd.cpp \
    newmainwindow.cpp

FORMS += \
    form.ui \
    mainwindow.ui \
    newmainwindow.ui

RC_FILE = app.rc

RESOURCES += \
    res.qrc
