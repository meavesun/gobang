#include "newmainwindow.h"
void GameModel::startGame(GameType type){
    gameType = type;
    //初始化棋盘
    gameMapVec.clear();
    for(int i=0;i<Tsize;i++){
        std::vector<int> lineBoard;
        for(int j=0;j<Tsize;j++){
            lineBoard.push_back(0);
        }
        gameMapVec.push_back(lineBoard);
    }
    //如果是AI模式，需要初始化评分数组
    if(gameType == AI){
        scoreMapVec.clear();
        for(int i=0;i<Tsize;i++){
            std::vector<int> lineScores;
            for(int j=0;j<Tsize;j++){
                lineScores.push_back(0);
            }
            scoreMapVec.push_back(lineScores);
        }
    }
    //输到黑方下棋为true，反之
    playerFlag = true;
}
void GameModel::actionByPerson(int row, int col){
    updateGameMap(row,col);
}
void GameModel::updateGameMap(int row, int col){
    if(playerFlag){
        gameMapVec[row][col] = 1;
    }else{
        gameMapVec[row][col] = -1;
    }
    //换手
    playerFlag = !playerFlag;
}

bool GameModel::isWin(int row,int col){
    //判断下棋点的水平、垂直、左斜、右斜方中有沒有5子相连的情況，如有则贏
    for(int i=0;i<5;i++){
        //先判断水平方向是否有5子相连
        if(row>0 && row<Tsize &&
           col-i>0 && col-i+4<Tsize &&
           gameMapVec[row][col-i] == gameMapVec[row][col-i+1] &&
           gameMapVec[row][col-i] == gameMapVec[row][col-i+2] &&
           gameMapVec[row][col-i] == gameMapVec[row][col-i+3] &&
           gameMapVec[row][col-i] == gameMapVec[row][col-i+4]){
            return true;
        }
        //先判断垂直方向是否有5子相连
        if(row-i>0 && row-i+4<Tsize &&
           col>0 && col<Tsize &&
           gameMapVec[row-i][col] == gameMapVec[row-i+1][col] &&
           gameMapVec[row-i][col] == gameMapVec[row-i+2][col] &&
           gameMapVec[row-i][col] == gameMapVec[row-i+3][col] &&
           gameMapVec[row-i][col] == gameMapVec[row-i+4][col]){
            return true;
        }
        //先判断"/"方向是否有5子相连，左下->右上
        if(
           row-i>0 && row-i+4<Tsize &&
           col+i-4>0 && col+i<Tsize &&
           gameMapVec[row-i][col+i] == gameMapVec[row-i+1][col+i-1] &&
           gameMapVec[row-i][col+i] == gameMapVec[row-i+2][col+i-2] &&
           gameMapVec[row-i][col+i] == gameMapVec[row-i+3][col+i-3] &&
           gameMapVec[row-i][col+i] == gameMapVec[row-i+4][col+i-4]){
            return true;
        }
        //先判断"\"方向是否有5子相连，右下->左上
        if(row-i>0 && row-i+4<Tsize &&
           col-i>0 && col-i+4<Tsize &&
           gameMapVec[row-i][col-i] == gameMapVec[row-i+1][col-i+1] &&
           gameMapVec[row-i][col-i] == gameMapVec[row-i+2][col-i+2] &&
           gameMapVec[row-i][col-i] == gameMapVec[row-i+3][col-i+3] &&
           gameMapVec[row-i][col-i] == gameMapVec[row-i+4][col-i+4]){
            return true;
        }

    }
    return false;
}
