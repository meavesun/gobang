﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "GameWnd.h"
#include <QMainWindow>

namespace Ui {
class MainWindow;
}



class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();


	QString GetIP();

protected:
    bool eventFilter(QObject *target, QEvent *event);

private slots:
    void on_btnConnect_clicked();
    void on_btnSend_clicked();

    void slotOutput(QString text);
    void slotUpdateRoomState();

private:
    Ui::MainWindow *ui;
    GameWnd* mGameWindow;
};

#endif
