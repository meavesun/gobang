﻿#ifndef TCPCLIENTSOCKET_H
#define TCPCLIENTSOCKET_H

#include <QTcpSocket>
#include <QDateTime>
#include "common.h"



class Player : public QObject
{
    Q_OBJECT
public:
    Player(QTcpSocket* socket);

    bool Send(MSG_HEADER msg);								//给玩家发简单TCP消息，只有消息头即可
    bool Send(MSG_HEADER msg, char* pData, int nDataLen);   //给玩家发复杂TCP消息，含消息头和附加nDataLen字节的数据

	bool IsHeartbeatTimeout();		//是否心跳超时？
	void UpdateHeartbeat();			//更新心跳

signals:
	void sigRecvMsg(char *pData, int nDataLen);		

protected slots:
    void slotTcpMsg();

public:
    QString mName;              //玩家名称，这里用作唯一标识(实际项目应该用真正唯一的ID)
    int mRoom;                  //房间号
    bool mBlack;                //当前执棋方 true=黑方

private:
	QTcpSocket* mSocket;        //tcpsocket对象，用来与TCP客户端通信
	QDateTime mHeartBeatTime;   //最近一次心跳时间，超过多少秒认为连接中断
	QByteArray mData;           //缓存接收的客户端数据，解决TCP粘包问题，当满足一个消息能解时再取出来解

};

#endif 
