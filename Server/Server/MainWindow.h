﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    QString GetLocalIP();
    void AddLog(QString text);

public slots:
    void slotLog(QString text);

private slots:
    void on_btnStart_clicked();

private:
    Ui::MainWindow *ui;
};

#endif
