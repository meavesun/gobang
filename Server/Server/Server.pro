# ----------------------------------------------------
# This file is generated by the Qt Visual Studio Add-in.
# ------------------------------------------------------

QT += core network widgets gui

TEMPLATE = app

CONFIG += debug_and_release
CONFIG(debug, debug|release){
    TARGET = ../../_debug/FIRServer
} else {
    TARGET = ../../_release/FIRServer
}

MOC_DIR += GeneratedFiles
OBJECTS_DIR += GeneratedFiles
UI_DIR += GeneratedFiles
RCC_DIR += GeneratedFiles


HEADERS += \
    ../Common/common.h \
    MainWindow.h \
    Player.h \
    Server.h

SOURCES += \
    main.cpp \
    MainWindow.cpp \
    Server.cpp \
    Player.cpp

FORMS += \
    mainwindow.ui

