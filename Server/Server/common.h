﻿#pragma once
#pragma pack(1)


//这是我的云服务器的公网IP，大家可以连上来，2个人进到同一个房间时就可以一起玩游戏了
#define FIR_SERVER_IP       "43.142.101.58"
#define FIR_SERVER_PORT		7777
#define FIR_DEFAULT_ROOM    1
#define FIR_MAX_ROOM		100     //房间数固定100个


//TCP消息定义
enum MSG_TYPE
{
    MQ_LOGON = 2020,    //客户端登录
    MA_LOGON,           //登录回复

    MQ_MOVE,            //客户端发送走一步数据
    MA_MOVE,            //服务端下发

    MQ_SUCCESS,         //胜利消息
    MA_START,           //对局开始

    MQ_SAY,             //玩家说话
    MA_SAY,

    MQ_HEARTBEAT,       //心跳包
    MA_HEARTBEAT,

    MA_UPDATEROOMSTATE, //服务器更新房间信息

    MA_OFFLINE,

}; 

//消息头
typedef struct _MSG_HEADER{
    int 	nType;          //消息类型 即上面的MSG_TYPE
    int 	nDataLen;       //本MSG_HEADER 之后接的额外数据的长度
    int     nNum1;          //用来传递参数，可以多定义几个，这样很多消息的参数一般用MSG_HEADER就可以传递完，不需要后面额外接数据
    int     nNum2;
    int     nNum3;
    int     nNum4;
    int     nNum5;
    char    szName[40];     //客户端name
    char    szSay[200];     //客户端说话内容
}MSG_HEADER, *PMSG_HEADER;


//1个房间的状态
typedef struct _ROOM_STATE{
	int 	nPlayers;		//当前人数 0、1、2
	char    szName1[40];    //客户端1 name
	char    szName2[40];    //客户端2 name
	_ROOM_STATE() {
		nPlayers = 0;
		memset(szName1, 0, 40);
		memset(szName2, 0, 40);
	}
}ROOM_STATE;


#pragma pack()
