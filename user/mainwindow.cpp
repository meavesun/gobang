#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPainter>
#include <QMouseEvent>
#include <cmath>
#include <QMessageBox>
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setFixedSize(
     blank*2+Gsize*Tsize,
     blank*2+Gsize*Tsize);
    initGame();
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::paintEvent(QPaintEvent* event){
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing); //设置抗锯齿

    for(int i=0;i<Tsize+1;i++){
        //从左到右，第(i+1)条竖线
        painter.drawLine(blank+Gsize*i,blank,
                         blank+Gsize*i,this->height()-blank);
        //从上到下，第(i+1)条橫线
        painter.drawLine(blank,blank+Gsize*i,
                         this->width()-blank,blank+Gsize*i);
    }
    QBrush brush;
    brush.setStyle(Qt::SolidPattern);
    //绘制落子标记(防止鼠标出框越界)
    if(clickPosRow>0 && clickPosRow<Tsize &&
       clickPosCol>0 && clickPosCol<Tsize &&
        game->gameMapVec[clickPosRow][clickPosCol]==0){

        if(game->playerFlag){
            brush.setColor(Qt::black);
        }else{
            brush.setColor(Qt::white);
        }
        painter.setBrush(brush);
        painter.drawRect(blank+Gsize*clickPosCol-slen/2,blank+Gsize*clickPosRow-slen/2,slen,slen);
    }
    for(int i=0;i<Tsize;i++){
        for(int j=0;j<Tsize;j++){
            if(game->gameMapVec[i][j]==1){
                brush.setColor(Qt::black);
                painter.setBrush(brush);
                painter.drawEllipse(blank+Gsize*j-radius,blank+Gsize*i-radius,radius*2,radius*2);
            }else if(game->gameMapVec[i][j]==-1){
                brush.setColor(Qt::white);
                painter.setBrush(brush);
                painter.drawEllipse(blank+Gsize*j-radius,blank+Gsize*i-radius,radius*2,radius*2);

            }

        }
    }
    if(clickPosCol>0 && clickPosCol<Tsize &&
       clickPosRow>0 && clickPosRow<Tsize &&
       (game->gameMapVec[clickPosRow][clickPosCol]==1||game->gameMapVec[clickPosRow][clickPosCol]==-1)){  //代碼解析：game->gameMapVec[clickPosRow][clickPosCol]==1||game->gameMapVec[clickPosRow][clickPosCol]==-1，防止因為5個0(空白)相連也被判勝利
        if(game->isWin(clickPosRow,clickPosCol) && game->gameStatus == PLAYING){
            game->gameStatus = WIN;
            QString str;
            str = game->gameMapVec[clickPosRow][clickPosCol]==1?"黑棋":"白棋";
            QMessageBox::StandardButton btnValue = QMessageBox::information(this,"五子棋嬴家",str+"胜利");
            if(btnValue == QMessageBox::Ok){
                GameType gameType =AI;
                game->startGame(gameType);
                game->gameStatus = PLAYING;
            }

        }
    }
}
void MainWindow::initGame(){
    game = new GameModel();
    initAIGame();
}
void MainWindow::initAIGame(){
    GameType gameType = AI;
    game->startGame(gameType);
    game->gameStatus=PLAYING;
    update();}
void MainWindow::mouseMoveEvent(QMouseEvent* event){
    //通过鼠标的hover确定落子的标记
    int x = event->x();
    int y = event->y();

    //棋盘边缘不能落子
    if(x>=blank+Gsize/2&&
           x<this->width()-blank-Gsize/2&&
           y>=blank+Gsize/2&&
           y<this->height()-blank-Gsize/2){
        //获取最近的左上角的点
        int col = (x-blank)/Gsize;
        int row = (y-blank)/Gsize;

        int leftTopPosX = blank+Gsize*col;
        int leftTopPosY = blank+Gsize*row;

        //根据距离算出合适的点击位置，一共四个点，根据半径距离选最近的
        clickPosRow = -1; //初始化最终值
        clickPosCol = -1;
        int len = 0;  //计算完后取整就可以了

        selectPos = false;

        //确定一个误差在范围内的点，且只可能确定一个出来
        //len：与左上角的点的距离
        len = sqrt((x-leftTopPosX)*(x-leftTopPosX)+(y-leftTopPosY)*(y-leftTopPosY));
        if(len<Bdis){
            clickPosRow = row;
            clickPosCol = col;
            if(game->gameMapVec[clickPosRow][clickPosCol]==0){
                selectPos = true;
            }
        }
        //len：与右上角的点的距离
        len = sqrt((x-leftTopPosX-Gsize)*(x-leftTopPosX-Gsize)+(y-leftTopPosY)*(y-leftTopPosY));
        if(len<Bdis){
            clickPosRow = row;
            clickPosCol = col+1;
            if(game->gameMapVec[clickPosRow][clickPosCol]==0){
                selectPos = true;
            }
        }
        //len：与左上角的点的距离
        len = sqrt((x-leftTopPosX)*(x-leftTopPosX)+(y-leftTopPosY-Gsize)*(y-leftTopPosY-Gsize));
        if(len<Bdis){
            clickPosRow = row+1;
            clickPosCol = col;
            if(game->gameMapVec[clickPosRow][clickPosCol]==0){
                selectPos = true;
            }
        }
        //len：与右上角的点的距离
        len = sqrt((x-leftTopPosX-Gsize)*(x-leftTopPosX-Gsize)+(y-leftTopPosY-Gsize)*(y-leftTopPosY-Gsize));
        if(len<Bdis){
            clickPosRow = row+1;
            clickPosCol = col+1;
            if(game->gameMapVec[clickPosRow][clickPosCol]==0){
                selectPos = true;
            }
        }
    }
    //存了坐标后也要重绘
    update();
}
GameType gameType =AI;

void MainWindow::mouseReleaseEvent(QMouseEvent* event){
    if(selectPos == false){
        return;
    }else{
        selectPos = false;
    }
    //由人来下棋
    chessOneByPerson();
    if(gameType == AI){ //人机模式
        //AI 下棋

    }
}

void MainWindow::chessOneByPerson(){
    if(clickPosRow!=-1 && clickPosCol!=-1 && game->gameMapVec[clickPosRow][clickPosCol]==0){
        //在游戏的数据模型中落子
        game->actionByPerson(clickPosRow,clickPosCol);
        //播放落子音效

        //重绘
        update();

    }
}
