#include "aimainwindow.h"
#include "ui_aimainwindow.h"
#include "common.h"
#include "Client.h"
#include <QIntValidator>
#include <QMessageBox>
#include <QMouseEvent>
#include <QPainter>
#include <QNetworkInterface>
#include "GameWnd1.h"


//注意：连接服务器的代码在子界面WidgetGame构造函数内

AIMainWindow::AIMainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::AIMainWindow)
{
    ui->setupUi(this);

    //ui->editIP->setText(GetIP());
    ui->editIP->setText(FIR_SERVER_IP);
    ui->editPort->setText(QString::number(FIR_SERVER_PORT));
    ui->editRoom->setText(QString::number(FIR_DEFAULT_ROOM));
    ui->editInput->installEventFilter(this);

    //限制端口编辑框输入格式：0-65535之间的整数
    QIntValidator* pInt = new QIntValidator(0, 65535, this);
    ui->editPort->setValidator(pInt);

    //房间号可输 0-99
    QIntValidator* pInt2 = new QIntValidator(0, 99, this);
    ui->editRoom->setValidator(pInt2);

    mGameWindow1 = ui->gameWnd1;
    connect(mGameWindow1,&GameWnd1::sigOutput,this,&AIMainWindow::slotOutput);
    connect(mGameWindow1,&GameWnd1::sigUpdateRoomState,this,&AIMainWindow::slotUpdateRoomState);
}

AIMainWindow::~AIMainWindow()
{
    delete ui;
}

QString AIMainWindow::GetIP()
{
    QList<QHostAddress> list = QNetworkInterface::allAddresses();
    foreach(QHostAddress address, list)
    {
        if (address.protocol() == QAbstractSocket::IPv4Protocol)
        {
            return address.toString();
        }
    }
    return "";
}

void AIMainWindow::on_btnConnect_clicked()
{
    QString ip = ui->editIP->text();
    int port = ui->editPort->text().toInt();
    int room = ui->editRoom->text().toInt();
    mGameWindow1->Connect(ip, port, room);
}

void AIMainWindow::on_btnSend_clicked()
{
    QString text = ui->editInput->toPlainText();
    mGameWindow1->Say(text);
    ui->editInput->clear();
}

void AIMainWindow::slotOutput(QString text)
{
    ui->editShow->append(text);
}

void AIMainWindow::slotUpdateRoomState()
{
    QList<ROOM_STATE> rooms;
    mGameWindow1->GetRoomState(rooms);

    ui->listRoom->clear();
    for (int i = 0; i < rooms.size(); i++)
    {
        ROOM_STATE r = rooms.at(i);
        QString name1 = QString::fromLocal8Bit(r.szName1);
        QString name2 = QString::fromLocal8Bit(r.szName2);

        QString text;
        int nPlayerInRoom = r.nPlayers;
        if(nPlayerInRoom == 0)
        {
            text = QStringLiteral("房间%1：空闲").arg(i);
        }
        else if(nPlayerInRoom == 1)
        {

            text = QStringLiteral("房间%1：一位玩家已进入(%2)").arg(i).arg(name1);
        }
        else
        {
            text = QStringLiteral("房间%1：房间已满！%2、%3").arg(i).arg(name1).arg(name2);
        }

        ui->listRoom->addItem(text);
    }
}

bool AIMainWindow::eventFilter(QObject *target, QEvent *event)
{
    if (target == ui->editInput)
    {
        if (event->type() == QEvent::KeyPress)
        {
            QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
            if (keyEvent->key() == Qt::Key_Enter || keyEvent->key() == Qt::Key_Return)
            {
                on_btnSend_clicked();
                return true;
            }
        }
    }
    return QMainWindow::eventFilter(target, event);
}
