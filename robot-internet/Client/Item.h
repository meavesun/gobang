﻿#pragma once
#include "qvector.h"

#include <QPoint>



class Item
{
public:
	Item(void);
	Item(QPoint pt,bool bBlack);
	~Item(void);

    bool operator==(const Item &item)const
	{
        return (mPt == item.mPt) && (mBlack == item.mBlack);
	}  

    QPoint mPt;
	bool mBlack;
private:
	

};

