

QT+=widgets
QT += core network widgets gui



HEADERS += \
    ../Common/common.h \
    Client.h \
    GameWnd1.h \
    Item.h \
    aimainwindow.h \
    form.h \
    mainwindow.h \
    GameWnd.h \
    newmainwindow.h

SOURCES += \
    Client.cpp \
    GameModel.cpp \
    GameWnd1.cpp \
    Item.cpp \
    aimainwindow.cpp \
    form.cpp \
    main.cpp \
    mainwindow.cpp \
    GameWnd.cpp \
    newmainwindow.cpp

FORMS += \
    form.ui \
    mainwindow.ui \
    newmainwindow.ui\
    aimainwindow.ui

RC_FILE = app.rc

RESOURCES += \
    res.qrc



