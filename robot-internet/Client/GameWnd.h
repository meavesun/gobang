﻿#ifndef GameWnd_H
#define GameWnd_H

#include "Client.h"
#include "Item.h"
#include <QFrame>


//棋盘行、列数，以及每个单元格尺寸
#define CHESS_ROWS		20
#define CHESS_COLUMES	20
#define RECT_WIDTH		36
#define RECT_HEIGHT		36


class GameWnd : public QFrame
{
    Q_OBJECT

public:
    explicit GameWnd(QWidget *parent = 0);
    ~GameWnd();

    void Connect(QString ip, int port, int room);
    void Say(QString text);
    void Output(QString text);
    void GetRoomState(QList<ROOM_STATE>& state);

signals:
    void sigOutput(QString text);
    void sigUpdateRoomState();

public slots:
    void slotOtherUserMove(int x, int y);
    void slotNewGame();

protected:
    void paintEvent(QPaintEvent *);
    void mousePressEvent(QMouseEvent *);

private:
    void DrawChessboard();
    void DrawItems();
    void DrawItemWithMouse();
    void DrawChessAtPoint(QPainter& painter,QPoint& pt);
    //统计某个方向(共8个方向)上的相连个数，用QPoint表示统计方向，如(1,1)表示右下方,（-1,0）表示向左
    int CountNearItem(Item item,QPoint ptDirection);

private:
    Client* mClient;
    QVector<Item> mItems;
    bool mBlack;     //当前该黑棋下
    QPoint mPos;
};

#endif
