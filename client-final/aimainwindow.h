#ifndef AIMAINWINDOW_H
#define AIMAINWINDOW_H


#include "GameWnd1.h"
#include <QMainWindow>

namespace Ui {
class AIMainWindow;
}



class AIMainWindow : public QMainWindow
{
    Q_OBJECT

public:
    AIMainWindow(QWidget *parent = nullptr);
    ~AIMainWindow();


    QString GetIP();

protected:
    bool eventFilter(QObject *target, QEvent *event);

private slots:
    void on_btnConnect_clicked();
    void on_btnSend_clicked();

    void slotOutput(QString text);
    void slotUpdateRoomState();

private:
    Ui::AIMainWindow *ui;
    GameWnd1* mGameWindow1;

};

#endif // AIMAINWINDOW_H
