#include "form.h"
#include "ui_form.h"
#include "mainwindow.h"
#include "newmainwindow.h"
#include "aimainwindow.h"
Form::Form(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form)
{
    ui->setupUi(this);
}

Form::~Form()
{
    delete ui;
}

void Form::on_pushButton_clicked()
{
    this->close();
        MainWindow *pic = new MainWindow;
        pic->show();

}


void Form::on_pushButton_2_clicked()
{
    newMainWindow *new_win = new newMainWindow;
    new_win->show();
}


void Form::on_pushButton_3_clicked()
{
    AIMainWindow *new_win = new AIMainWindow;
    new_win->show();
}

