#ifndef NEWMAINWINDOW_H
#define NEWMAINWINDOW_H
#include <QMainWindow>
#include <QWidget>
#include <QPainter>
#include <vector>
#include "form.h"
const int Tsize = 15;
const int blank = 30; //棋盘边缘空隙
const int radius = 15; //棋子半径
const int slen = 6; //落子标记边长
const int Gsize = 40; //格子大小
const int Bdis = Gsize*0.4; //鼠标点击的模糊距离上限

enum GameType{
    MAN,
    AI  //人机对战模式
};

enum GameStatus
{
    PLAYING,
    WIN,
    DEAD
};
class GameModel{
public:
    GameModel(){};
public:
    //储存当前游戏棋盘和棋子的情况，空白为0，黑子1，白子-1
    std::vector<std::vector<int>> gameMapVec;

    //储存各个位点的评分情况，作为AI下棋的依据
    std::vector<std::vector<int>> scoreMapVec;

    //标志下棋方，true黑
    bool playerFlag;
    GameStatus gameStatus; //游戏状态
    GameType gameType = AI;
    void startGame(GameType type);
    void actionByPerson(int row, int col);
    void updateGameMap(int row, int col);
    bool isWin(int row,int col);

};
namespace Ui {
class newMainWindow;
}

class newMainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit newMainWindow(QWidget *parent = nullptr);
    ~newMainWindow();
    newMainWindow *new_win;

private:
    Ui::newMainWindow *ui;
    void paintEvent(QPaintEvent* event);
    GameModel *game;
    void initGame();
    void initAIGame();
    void mouseMoveEvent(QMouseEvent* event);
    void mouseReleaseEvent(QMouseEvent* event);
    void chessOneByPerson();
    int clickPosRow, clickPosCol;
    bool selectPos =false;
};

#endif // NEWMAINWINDOW_H
